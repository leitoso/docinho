defmodule Docinho.Responders.Xkcd do
  @moduledoc false

  use Hedwig.Responder
  require Logger

  @usage """
    /xkcd (id) - mostra o xkcd por id ou o mais recente
  """
  hear ~r/^\/xkcd (.*)$/, msg do
    get_xkcd({:ok, %{"num" => msg.matches[1]}})
      |> reply_xkcd(msg)
  end

  hear ~r/^\/xkcd$/, msg do
    case HTTPoison.get("https://xkcd.com/info.0.json") do
      {:ok, %{status_code: 200, body: body}} ->
        Poison.decode(body)
          |> get_xkcd
          |> reply_xkcd(msg)
      {:ok, _} ->
        Logger.error("[xkcd] Something happened")
      {:error, %{reason: reason}} ->
        Logger.error("[xkcd] #{inspect(reason)}")
    end
  end

  defp get_xkcd {:ok, %{"num" => id}} do
    case HTTPoison.get("https://xkcd.com/#{id}/info.0.json") do
      {:ok, %{status_code: 200, body: body}} ->
        Poison.decode(body)
      {:ok, _} ->
        Logger.error("[xkcd] Something happened while getting xkcd #{id}")
      {:error, %{reason: reason}} ->
        Logger.error("[xkcd] #{inspect(reason)}")
    end
  end

  defp reply_xkcd {:ok, %{"title" => title, "img" => img}}, msg do
    reply msg, "#{title} - #{img}"
  end
end
