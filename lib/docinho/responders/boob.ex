defmodule Docinho.Responders.Boob do
  @moduledoc false

  use Hedwig.Responder
  require Logger

  @usage """
    /peitos - mostra peitos aleatórios
  """
  hear ~r/^\/(boobs|peito|peitos|teta|tetas)$/, msg do
    case HTTPoison.get("http://api.oboobs.ru/noise/1") do
      {:ok, %{status_code: 200, body: body}} ->
        Poison.decode(body)
          |> preview_reply(msg)
      {:ok, _} ->
        Logger.error("[boobs] Something happened")
      {:error, %{reason: reason}} ->
        Logger.error("[boobs] #{inspect(reason)}")
    end
  end

  defp preview_reply {:ok, [%{"preview" => fragment, "id" => _}]}, msg do
    reply msg, "https://media.oboobs.ru/#{fragment}"
  end
end
