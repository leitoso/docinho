defmodule Docinho.Responders.Butt do
  @moduledoc false

  use Hedwig.Responder
  require Logger

  @usage """
    /bunda - mostra bundas aleatórias
  """
  hear ~r/^\/(ass|bunda|bundas)$/, msg do
    case HTTPoison.get("http://api.obutts.ru/noise/1") do
      {:ok, %{status_code: 200, body: body}} ->
        Poison.decode(body)
          |> preview_reply(msg)
      {:ok, _} ->
        Logger.error("[butts] Something happened")
      {:error, %{reason: reason}} ->
        Logger.error("[butts] #{inspect(reason)}")
    end
  end

  defp preview_reply {:ok, [%{"preview" => fragment, "id" => _}]}, msg do
    reply msg, "https://media.obutts.ru/#{fragment}"
  end
end
