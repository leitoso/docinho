# Docinho

Your favorite #NSFW useless bot ...

## Installation

Checkout this repo, change `config.exs` with whatever token botfather gave you and `lib/docinho/responders/weather.ex` openweather `@appid`.

Then proceed to install dependencies and running ...
```
$ mix deps.get
$ iex -S mix
```

## Usage

Proxy your telegram bot api calls thru port 4000 of your ip address.
Although I recommend using a third party server with fixed ip and ssl setup according to telegram api guidelines.

Last, make your bot join your chat or group and try using `/help` and check what it does.
