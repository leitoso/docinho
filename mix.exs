defmodule Docinho.MixProject do
  use Mix.Project

  def project do
    [
      app: :docinho,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      applications: [:hedwig_telegram],
      extra_applications: [:logger],
      mod: {Docinho.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [ 
      hedwig_telegram: "~> 0.1.0",
      cowboy: "~> 1.0",
      plug: "~> 1.1"
    ]
  end
end
